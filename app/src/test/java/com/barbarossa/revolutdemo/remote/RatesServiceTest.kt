package com.barbarossa.revolutdemo.remote

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.barbarossa.revolutdemo.data.remote.ApiSuccessResponse
import com.barbarossa.revolutdemo.data.remote.RatesService
import com.barbarossa.revolutdemo.util.LiveDataCallAdapterFactory
import com.barbarossa.revolutdemo.util.LiveDataTestUtil
import com.google.gson.GsonBuilder
import okhttp3.mockwebserver.MockResponse
import org.junit.Test

import org.junit.Before
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

import okhttp3.mockwebserver.MockWebServer
import okio.Okio
import org.hamcrest.CoreMatchers.`is`
import org.hamcrest.MatcherAssert.assertThat
import org.junit.After
import org.junit.Rule
import java.io.IOException
import java.nio.charset.StandardCharsets
import java.sql.Date


class RatesServiceTest {

    @get:Rule
    var instantTaskExecutorRule = InstantTaskExecutorRule()

    private var ratesService: RatesService? = null

    private var mockWebServer: MockWebServer? = null


    @Before
    fun createService() {
        val gson = GsonBuilder().setDateFormat("yyyy-MM-dd").create()

        mockWebServer = MockWebServer()
        ratesService = Retrofit.Builder()
            .baseUrl(mockWebServer?.url("/"))
            .addConverterFactory(GsonConverterFactory.create(gson))
            .addCallAdapterFactory(LiveDataCallAdapterFactory())
            .build()
            .create(RatesService::class.java!!)
    }

    @After
    @Throws(IOException::class)
    fun stopService() {
        mockWebServer?.shutdown()
    }

    @Test
    fun testGetRates() {
        enqueueResponse("response-EUR.json")

        // When making the call for the courses to the camp service
        val response = LiveDataTestUtil.getValue(ratesService?.getRates("EUR")!!)

        val request = mockWebServer?.takeRequest()
        assertThat<String>(request?.path, `is`<String>("/latest?base=EUR"))

        assertThat("Response was not successful", response is ApiSuccessResponse)

        with((response as ApiSuccessResponse).body) {
            assertThat("Incorrect base", base == "EUR")
            assertThat("Incorrect date", date == Date.valueOf("2018-09-06"))
            assertThat("Incorrect exchange map dimension", rates.size == 32)
            assertThat("Incorrect exchange map sample data", rates["RON"] == 4.6324f)
        }
    }



    @Throws(IOException::class)
    private fun enqueueResponse(fileName: String) {
        // Enqueue a response with empty headers
        enqueueResponse(fileName, emptyMap())
    }

    @Throws(IOException::class)
    private fun enqueueResponse(fileName: String, headers: Map<String, String>) {
        // Get an input stream from the file in the resource directory
        val inputStream = javaClass.classLoader
            .getResourceAsStream("api-sample/$fileName")

        // Returns a new source that buffers reads from source.
        // The returned source will perform bulk reads into its in-memory buffer.
        // Use this wherever you read a source to get an ergonomic and efficient access to data.
        val source = Okio.buffer(Okio.source(inputStream))

        val mockResponse = MockResponse()

        // Add headers to the mock response
        for ((key, value) in headers) {
            mockResponse.addHeader(key, value)
        }

        // Scripts response to be returned to a request made in sequence.
        // The first request is served by the first enqueued response;
        // the second request by the second enqueued response; and so on.
        mockWebServer?.enqueue(
            mockResponse
                .setBody(source.readString(StandardCharsets.UTF_8))
        )
    }

}
