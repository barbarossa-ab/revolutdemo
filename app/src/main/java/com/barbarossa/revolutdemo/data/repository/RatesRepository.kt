package com.barbarossa.revolutdemo.data.repository

import androidx.lifecycle.LiveData
import com.barbarossa.revolutdemo.data.model.Rates
import com.barbarossa.revolutdemo.data.remote.ApiResponse
import com.barbarossa.revolutdemo.data.remote.RatesService
import com.barbarossa.revolutdemo.data.remote.Resource

class RatesRepository (val service : RatesService) {

    companion object {
        const val T_REFRESH = 1000L
        const val BASE = "EUR"
    }

    private var base = BASE

    fun changeBase(base : String) {
        this.base = base
    }

    fun loadRates(): LiveData<Resource<Rates>> {
        return object : UpdatingResource<Rates>(T_REFRESH) {
            override fun createCall(): LiveData<ApiResponse<Rates>> {
                return service.getRates(base)
            }
        }.asLiveData()
    }

}