package com.barbarossa.revolutdemo.data.repository

import android.os.Handler
import android.os.Looper
import androidx.annotation.MainThread
import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import com.barbarossa.revolutdemo.data.remote.*

abstract class UpdatingResource <T> (
    private val periodMillis : Long
){
    private val handler = Handler(Looper.getMainLooper())

    private val result : MediatorLiveData <Resource<T>>
            = object : MediatorLiveData<Resource<T>>() {

        override fun onActive() {
            super.onActive()

            value = Resource.loading()
            handler.post(updater)
        }

        override fun onInactive() {
            super.onInactive()

            handler.removeCallbacksAndMessages(null)
        }
    }


    private val updater = object : Runnable {
        override fun run() {
            try {
                val apiResponse = createCall()

                result.addSource(apiResponse) { response ->
                    result.removeSource(apiResponse)

                    when (response) {
                        is ApiSuccessResponse -> {
                            updateValue(Resource.success(response.body))
                        }
                        is ApiEmptyResponse -> {
                            updateValue(Resource.error())
                        }
                        is ApiErrorResponse -> {
                            updateValue(Resource.error(response.errorMessage, null))
                        }
                    }
                }
            } catch (e : Exception) {
                e.message?.let {
                    updateValue(Resource.error(it))
                }
            }

            if(result.hasActiveObservers()) {
                handler.postDelayed(this, periodMillis)
            }
        }
    }

    fun asLiveData() = result as LiveData<Resource<T>>

    private fun updateValue(newValue: Resource<T>) {
        result.value = newValue
    }

    @MainThread
    protected abstract fun createCall(): LiveData<ApiResponse<T>>

}