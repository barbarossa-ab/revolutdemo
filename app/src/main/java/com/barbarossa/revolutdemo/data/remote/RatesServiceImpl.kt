package com.barbarossa.revolutdemo.data.remote

import com.barbarossa.revolutdemo.util.LiveDataCallAdapterFactory
import com.google.gson.GsonBuilder
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

object RatesServiceImpl {
    private var ratesService: RatesService? = null

    val instance: RatesService
        get() {
            if (ratesService == null) {
                val okHttpClient = OkHttpClient.Builder()
                    .retryOnConnectionFailure(false)
                    .build()

                val gson = GsonBuilder().setDateFormat("yyyy-MM-dd").create()
                ratesService = Retrofit.Builder()
                    .baseUrl(RatesService.ENDPOINT)
                    .addConverterFactory(GsonConverterFactory.create(gson))
                    .addCallAdapterFactory(LiveDataCallAdapterFactory())
                    .client(okHttpClient)
                    .build()
                    .create(RatesService::class.java)
            }
            return ratesService!!
        }
}
