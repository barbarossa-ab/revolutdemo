package com.barbarossa.revolutdemo.data.model

import java.sql.Date

data class Rates(
    val base : String,
    val date : Date,
    val rates : LinkedHashMap<String, Float>
)