package com.barbarossa.revolutdemo.data.remote

enum class Status {
    SUCCESS,
    ERROR,
    LOADING
}
