package com.barbarossa.revolutdemo.data.remote

import androidx.lifecycle.LiveData
import com.barbarossa.revolutdemo.data.model.Rates
import retrofit2.http.GET
import retrofit2.http.Query

interface RatesService {
    companion object {
        const val ENDPOINT = "https://revolut.duckdns.org/"
    }

    @GET("/latest")
    fun getRates(@Query(value = "base", encoded = true) base : String): LiveData<ApiResponse<Rates>>

}