package com.barbarossa.revolutdemo.util

import android.app.Application
import com.barbarossa.revolutdemo.DemoRatesApp
import com.barbarossa.revolutdemo.data.repository.RatesRepository
import com.barbarossa.revolutdemo.ui.RatesViewModel

object Injector {

    fun provideRatesRepository(application : Application) : RatesRepository{
        return (application as DemoRatesApp).repository
    }

    fun provideRatesViewModelFactory(application : Application) : RatesViewModel.Factory{
        return RatesViewModel.Factory(provideRatesRepository(application))
    }

}
