package com.barbarossa.revolutdemo.ui

import android.content.Context
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.rates_item.view.*
import java.util.*
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import com.bumptech.glide.Glide


class RatesAdapter
    (private val viewModel : RatesViewModel)
    : RecyclerView.Adapter<RecyclerView.ViewHolder>()
{
    companion object {
        const val TAG = "RatesAdapter"
    }

    private var items: MutableList<Exchange> = LinkedList()
    lateinit var recyclerView: RecyclerView

    init {
        setHasStableIds(true)
    }

    fun updateItems(newItems : List<Exchange> ) {
        items.clear()
        items.addAll(newItems)
        notifyDataSetChanged()
    }

    override fun getItemId(position: Int): Long {
        return items[position].currency.hashCode().toLong()
    }

    override fun onAttachedToRecyclerView(recyclerView: RecyclerView) {
        super.onAttachedToRecyclerView(recyclerView)

        this.recyclerView = recyclerView
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(com.barbarossa.revolutdemo.R.layout.rates_item, parent, false)
        return RateViewHolder(view)
    }

    override fun getItemCount(): Int {
        if(items == null) {
            return 0
        }
        return items!!.size
    }

    override fun getItemViewType(position: Int): Int {
        return if(position == 0)
            0
        else 1
    }

    var baseChange = false

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if(items == null) {
            return
        }
        val exchange = items!![position]

        with((holder as RateViewHolder).itemView) {
            currency.text = exchange.currency

            if(holder.itemViewType == 0) {
                sumText.addTextChangedListener(BaseTextWatcher(sumText))

                sumText.onFocusChangeListener = View.OnFocusChangeListener { _, hasFocus ->
                    val inputManager = context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager

                    if(hasFocus) {
                        inputManager.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0)
                    } else {
                        inputManager.hideSoftInputFromWindow(windowToken, 0)
                    }
                }

                if(baseChange) {
                    (sumText as TextView).text = exchange.sum.toString()

                    baseChange = false
                }

                this.setOnClickListener {
                    if(!sumText.hasFocus()) {
                        sumText.requestFocus()
                    } else {
                        currency.requestFocus()
                    }
                }

            } else {
                (sumText as TextView).text = "%.4f".format(exchange.sum)

                this.setOnClickListener {
                    changeBase(position)
                }

                sumText.onFocusChangeListener = View.OnFocusChangeListener { _, hasFocus ->
                    if(hasFocus) {
                        sumText.onFocusChangeListener = null
                        changeBase(position)
                    }
                }
            }

            setFlag(exchange)
        }
    }

    private fun View.setFlag(exchange: Exchange) {
        var drawableName = exchange.currency.toLowerCase()
        if (drawableName == "try") {
            drawableName = "_try"
        }
        val resId = resources.getIdentifier(drawableName, "drawable", context.packageName)

        Glide.with(context)
            .load(resId)
            .into(flag)
    }


    private fun changeBase(position : Int) {
        if(baseChange) {
            return
        }

        viewModel.onBaseIndexChange(position)

        notifyItemMoved(position, 0)
        val item = items.removeAt(position)
        items.add(0, item)

        recyclerView.scrollToPosition(0)

        baseChange = true
    }

    class RateViewHolder (view : View): RecyclerView.ViewHolder(view)

    inner class BaseTextWatcher(private val sumText : EditText): TextWatcher {
        override fun afterTextChanged(s: Editable) {
            Log.d(TAG, "afterTextChanged : $s")

            if (s != null && s.isNotEmpty()) {
                if (s.toString().length > 1 && s.toString().startsWith("0")) {
                    (sumText as TextView).text = "0"
                }
                viewModel.onSumChange(s.toString().toFloat())

            } else {
                viewModel.onSumChange(1f)
            }
        }

        override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {
        }

        override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
        }
    }
}