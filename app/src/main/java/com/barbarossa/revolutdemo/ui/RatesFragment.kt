package com.barbarossa.revolutdemo.ui

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.barbarossa.revolutdemo.R
import com.barbarossa.revolutdemo.data.remote.RatesService
import com.barbarossa.revolutdemo.data.repository.RatesRepository
import com.barbarossa.revolutdemo.util.Injector
import com.barbarossa.revolutdemo.util.LiveDataCallAdapterFactory
import com.google.gson.GsonBuilder
import kotlinx.android.synthetic.main.rates_fragment.view.*
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class RatesFragment : Fragment() {



    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.rates_fragment, container, false)

        val factory = Injector.provideRatesViewModelFactory(activity?.application!!)
        val viewModel = ViewModelProviders.of(this, factory)
            .get(RatesViewModel::class.java)

        val adapter = RatesAdapter(viewModel)
        view.ratesRecycleView.adapter = adapter

        viewModel.exchangeValues.observe(this, Observer {
            it ?.let{ list ->
                Log.d(RatesViewModel.TAG, "Fragment received values : $list")

                adapter.updateItems(list)
            }
        })

        return view
    }


}