package com.barbarossa.revolutdemo.ui

import androidx.recyclerview.widget.DiffUtil

data class Exchange (
    val currency: String,
    var sum : Float
)

//class ExchangeDiffCallback(private val oldExchangeList: List<Exchange>, private val newExchangeList: List<Exchange>) :
//    DiffUtil.Callback() {
//
//    override fun getOldListSize(): Int {
//        return oldExchangeList.size
//    }
//
//    override fun getNewListSize(): Int {
//        return newExchangeList.size
//    }
//
//    override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
//        return oldExchangeList[oldItemPosition].currency == newExchangeList[newItemPosition].currency
//    }
//
//    override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
//        val oldEmployee = oldExchangeList[oldItemPosition]
//        val newEmployee = newExchangeList[newItemPosition]
//
//        return oldEmployee.sum == newEmployee.sum
//    }
//
////    @Nullable
////    override fun getChangePayload(oldItemPosition: Int, newItemPosition: Int): Any? {
////        return super.getChangePayload(oldItemPosition, newItemPosition)
////    }
//}