package com.barbarossa.revolutdemo.ui

import android.util.Log
import androidx.lifecycle.*
import androidx.lifecycle.Observer
import com.barbarossa.revolutdemo.data.model.Rates
import com.barbarossa.revolutdemo.data.remote.Resource
import com.barbarossa.revolutdemo.data.repository.RatesRepository
import java.util.*

class RatesViewModel (private val repository: RatesRepository): ViewModel() {

    companion object {
        const val TAG = "RatesViewModel"
    }

    private val _rates : LiveData<Resource<Rates>> = repository.loadRates()
    private val _sum = MutableLiveData<Float>()
    private val _base = MutableLiveData<String>()


    private var currentList : LinkedList<Exchange>? = null

    private val _exchangeValues = MediatorLiveData<LinkedList<Exchange>>()
    val exchangeValues : LiveData<LinkedList<Exchange>>
        get() = _exchangeValues


    private var rates : Rates? = null


    private var onRatesChangeObserver = Observer<Resource<Rates>> { resource ->
        resource?.data?.let {
            this.rates = it

//            Log.d(TAG, "onRatesChangeObserver, rates = $it")
            calculateExchange()
        }
    }

    private var onSumChangeObserver = Observer<Float> { sum ->
//        Log.d(TAG, "onSumChangeObserver, sum = $sum")
        _exchangeValues.value?.let {
            it[0]?.sum = sum
        }
        calculateExchange()
    }

    private var onBaseChangeObserver = Observer<String> { base ->
//        Log.d(TAG, "onBaseChangeObserver() start : ${_exchangeValues.value}")

        // reorder list
        val newList = LinkedList<Exchange>()

        currentList?.let {
            it.find {xcg ->
                xcg.currency == base

            }?.let { baseItem ->
                newList.add(Exchange(baseItem.currency, baseItem.sum))

                it.forEach { item ->
                    if(item.currency != baseItem.currency) {
                        newList.add(Exchange(item.currency, item.sum))
                    }
                }

                currentList = newList
                _exchangeValues.postValue(currentList)

                repository.changeBase(base)
            }
        }
    }

    init {
        _sum.value = 1f
        _base.value = "EUR"

        _exchangeValues.addSource(_rates, onRatesChangeObserver)
        _exchangeValues.addSource(_sum, onSumChangeObserver)
        _exchangeValues.addSource(_base, onBaseChangeObserver)
    }

    override fun onCleared() {
        _exchangeValues.removeSource(_rates)
        _exchangeValues.removeSource(_sum)
        _exchangeValues.removeSource(_base)
    }


    fun onBaseIndexChange(baseIndex : Int) {
        currentList?.let {
            onBaseChange(it[baseIndex].currency)
        }
    }


    private fun onBaseChange(base : String) {
        if(_base.value != base) {
            _base.postValue(base)
        }
    }


    fun onSumChange(sum : Float) {
        if((_base.value == currentList!![0].currency) && _sum.value != sum) {
            _sum.postValue(sum)
        }
    }


    private fun calculateExchange() {
//        Log.d(TAG, "calculateExchange() start : ${_exchangeValues.value}")

        rates?.let {
            val list: LinkedList<Exchange> = LinkedList()

            if(currentList == null) {
                list.add(Exchange(it.base, 1f))
                it.rates.asIterable().forEach { xch ->
                    list.add(Exchange(xch.key, xch.value))
                }
            } else {
                if(currentList!![0].currency != it.base) {
                    return
                }

                list.add(Exchange(currentList!![0].currency, currentList!![0].sum))
                val sum = currentList!![0].sum
                currentList!!.drop(1).forEach { item ->

                    val currency = item.currency
                    list.add(Exchange(currency, sum * it.rates[currency]!!))
                }

//                Log.d(TAG, "calculateExchange() end, planned update : $list")
            }

            currentList = list
            _exchangeValues.postValue(list)
        }

    }


    class Factory(private var repository: RatesRepository) : ViewModelProvider.NewInstanceFactory() {
        override fun <T : ViewModel> create(modelClass: Class<T>): T {
            return RatesViewModel(repository) as T
        }
    }

}