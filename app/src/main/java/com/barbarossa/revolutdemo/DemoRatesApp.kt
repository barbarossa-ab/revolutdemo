package com.barbarossa.revolutdemo

import android.app.Application
import com.barbarossa.revolutdemo.data.remote.RatesService
import com.barbarossa.revolutdemo.data.remote.RatesServiceImpl
import com.barbarossa.revolutdemo.data.repository.RatesRepository

class DemoRatesApp : Application() {

    lateinit var ratesService : RatesService
    lateinit var repository : RatesRepository

    override fun onCreate() {
        super.onCreate()

        ratesService = RatesServiceImpl.instance
        repository = RatesRepository(ratesService)
    }
}